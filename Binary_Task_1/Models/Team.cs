﻿using System;
using Newtonsoft.Json;

namespace Binary_Task_1.Models
{
    class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nName: {Name}\nCreatedAt: {CreatedAt}\n";
        }
    }
}
