﻿using Newtonsoft.Json;

namespace Binary_Task_1.Models
{
    class TaskStateModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
