﻿using System;
using Newtonsoft.Json;

namespace Binary_Task_1.Models
{
    class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public States State { get; set; }

        [JsonProperty("project_id")]
        public int ProjectId { get; set; }

        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nName: {Name}\nDescription: {Description}\nCreatedAt: {CreatedAt}\nFinishedAt: {FinishedAt}" +
                $"\nState: {State}\nProjectId: {ProjectId}\nPerformedId: {PerformerId}\n";
        }
    }

    enum States {
        Created,
        Started,
        Finished,
        Canceled
    }
}
