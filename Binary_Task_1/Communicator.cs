﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Binary_Task_1.Models;
using Binary_Task_1.ExtraModels;

namespace Binary_Task_1
{
    class Communicator
    {
        public const string URL = "https://bsa2019.azurewebsites.net/api/";
        public const string PROJECTS = "Projects/";
        public const string TASKS = "Tasks/";
        public const string TASKSTATES = "TaskStates/";
        public const string TEAMS = "Teams/";
        public const string USERS = "Users/";

        public string GetStringFromApi(string route)
        {
            using (HttpClient client = new HttpClient())
            {
                var result = client.GetAsync(URL + route).GetAwaiter().GetResult();
                result.EnsureSuccessStatusCode();
                return result.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            }
        }

        public Dictionary<string, int> GetUserTaskCount(int performerId)
        {
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(GetStringFromApi(TASKS));
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(GetStringFromApi(PROJECTS));
            return projects.ToDictionary(p => p.Name, p => tasks.Where(t => t.PerformerId == performerId && t.ProjectId == p.Id).Count()).Where(kvp => kvp.Value != 0).ToDictionary(p=> p.Key, p=> p.Value);
        }

        public List<Task> GetTaskListByUser(int performerId)
        {
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(GetStringFromApi(TASKS));
            return tasks.Where(t => t.PerformerId == performerId && t.Name.Length < 45).ToList();
        }


        public List<Tuple<int, string>> GetFinishedTasks(int performerId)
        {
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(GetStringFromApi(TASKS));
            return tasks.Where(t => t.State == States.Finished && t.PerformerId == performerId).Select(t => Tuple.Create(t.Id, t.Name)).ToList();
        }

        public List<DetailedTeam> GetDetailedTeam()
        {
            List<Team> teams = JsonConvert.DeserializeObject<List<Team>>(GetStringFromApi(TEAMS));
            List<User> users = JsonConvert.DeserializeObject<List<User>>(GetStringFromApi(USERS));

            return teams.GroupJoin(users, t => t.Id, u => u.TeamId,
                (tm, us) => new DetailedTeam
                {
                    Id = tm.Id,
                    Name = tm.Name,
                    Users = us.Where(u => u.Birthday.AddYears(12) <= DateTime.Now && tm.Id == u.TeamId).OrderByDescending(p => p.RegisteredAt).ToList()
                }
                ).Where(u => u.Users.Count != 0).ToList();
        }

        public List<UserWithTasks> GetUserWithTasks()
        {
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(GetStringFromApi(TASKS));
            List<User> users = JsonConvert.DeserializeObject<List<User>>(GetStringFromApi(USERS));

            return users.GroupJoin(tasks, u => u.Id, t => t.PerformerId,
                (us, ts) => new UserWithTasks
                {
                    User = us,
                    Tasks = ts.Where(t => t.PerformerId == us.Id).OrderByDescending(t => t.Name.Length).ToList()
                }
                ).Where(u => u.Tasks.Count != 0).OrderBy(u => u.User.FirstName).ToList();
        }

        public DetailedUser GetDetailedUser(int id)
        {
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(GetStringFromApi(TASKS));
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(GetStringFromApi(PROJECTS));
            User user = JsonConvert.DeserializeObject<User>(GetStringFromApi(USERS + id));

            return tasks.Join(projects, t => t.ProjectId, p => p.Id, (pr, t) => new { pr, t }).Select(y => new DetailedUser
            {
                User = user,
                LastProject = projects.Select(p => p).Where(t => t.CreatedAt == projects.Max(d => d.CreatedAt)).SingleOrDefault(),
                TaskCount = tasks.Where(t => t.ProjectId == projects.Select(p => p).Where(p => p.CreatedAt == projects.Max(d => d.CreatedAt)).SingleOrDefault()?.Id).Count(),
                NotFinishedTaskCount = tasks.Where(t => t.State != States.Finished && t.PerformerId == user.Id).Count(),
                LongestTask = tasks.Where(t => t.FinishedAt - t.CreatedAt == tasks.Max(w => w.FinishedAt - w.CreatedAt)).SingleOrDefault()
            }).FirstOrDefault();
        }

        public DetailedProject GetDetailedProject(int id)
        {
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(GetStringFromApi(TASKS));
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(GetStringFromApi(PROJECTS));
            Project project = JsonConvert.DeserializeObject<Project>(GetStringFromApi(PROJECTS+id));
            List<User> users = JsonConvert.DeserializeObject<List<User>>(GetStringFromApi(USERS));

            var k = tasks.Where(yyy => yyy.ProjectId == id);

            return users.Join(tasks, u => u.Id, t => t.PerformerId,
                (us, ts) => new { us, ts }).Select(y => new DetailedProject
                {
                    Project = project,
                    LongestTask = tasks.Where(t => t.ProjectId == project.Id && t.Description.Length == tasks.Where(tt => tt.ProjectId == project.Id).Max(m => m.Description.Length)).FirstOrDefault(),
                    ShortestTask = tasks.Where(t => t.ProjectId == project.Id && t.Name.Length == tasks.Where(tt => tt.ProjectId == project.Id).Min(m => m.Name.Length)).FirstOrDefault(),
                    UserCount = projects.Where(p => p.Description.Length > 25 || tasks.Where(t => t.ProjectId == p.Id).Count() > 3).GroupJoin(tasks,p=>p.Id, t=> t.ProjectId, 
                    (p,t) => t.Where(dt=> p.Id == dt.ProjectId).Select(s=> s.PerformerId)
                    ).ToList().Distinct().Count()                    
                }
                ).FirstOrDefault();
        }
    }
}
