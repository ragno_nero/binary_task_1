﻿using Binary_Task_1.Models;
using System.Text;

namespace Binary_Task_1.ExtraModels
{
    class DetailedUser
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TaskCount { get; set; }
        public int NotFinishedTaskCount { get; set; }
        public Task LongestTask { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(User.ToString() + "\n");
            str.AppendLine($"TaskCount: {TaskCount}\nNotFinishedTaskCount: {NotFinishedTaskCount}\n  LastProject:");
            str.AppendLine(LastProject.ToString() + "\n  Longest Task:");
            str.AppendLine(LongestTask.ToString());
            return str.ToString();
        }
    }
}
