﻿using Binary_Task_1.Models;
using System.Text;

namespace Binary_Task_1.ExtraModels
{
    class DetailedProject
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int UserCount { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(Project.ToString());
            str.AppendLine($"\nUserCount: {UserCount}\n  LongestTask:");
            str.AppendLine(LongestTask.ToString() + $"  ShortestTask:\n{ShortestTask}");
            return str.ToString();
        }
    }
}
