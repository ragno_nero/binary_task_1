﻿
using Binary_Task_1.Models;
using System.Collections.Generic;
using System.Text;

namespace Binary_Task_1.ExtraModels
{
    class UserWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(User.ToString()+"\n  ListOfTasks:\n");
            foreach(var t in Tasks)
            {
                str.AppendLine(t.ToString());
                str.AppendLine("----------");
            }
            return str.ToString();
        }
    }
}
