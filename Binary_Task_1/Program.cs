﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using Binary_Task_1.Models;

namespace Binary_Task_1
{
    class Program
    {
        public const string URL = "https://bsa2019.azurewebsites.net/api/";
        public const string PROJECTS = "Projects/";
        public const string TASKS = "Tasks/";
        public const string TASKSTATES = "TaskStates/";
        public const string TEAMS = "Teams/";
        public const string USERS = "Users/";

        static void Main(string[] args)
        {
            Menu.Execute();
        }
    }
}
