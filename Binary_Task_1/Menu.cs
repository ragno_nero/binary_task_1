﻿using Binary_Task_1.ExtraModels;
using System;
using System.Collections.Generic;

namespace Binary_Task_1
{
    static class Menu
    {
        public static void Execute()
        {
            Communicator communicator = new Communicator();
            bool menu = true;
            do
            {
                try
                {
                    int choice = ShowMenu();
                    int id = 0;
                    switch (choice)
                    {
                        case 1:
                            id = GetId();
                            Dictionary<string, int> counts = communicator.GetUserTaskCount(id);
                            Console.WriteLine("----------------------------------------------------------");
                            if (counts != null)
                                foreach (var c in counts)
                                    Console.WriteLine($"Project: {c.Key}, Value: {c.Value}");
                            Console.WriteLine("----------------------------------------------------------");
                            break;
                        case 2:
                            id = GetId();
                            List<Models.Task> tasks = communicator.GetTaskListByUser(id);
                            PrintOnConsole<Models.Task>(tasks);
                            break;
                        case 3:
                            id = GetId();
                            List<Tuple<int, string>> finished = communicator.GetFinishedTasks(id);
                            PrintOnConsole<Tuple<int, string>>(finished);
                            break;
                        case 4:
                            List<DetailedTeam> ldt = communicator.GetDetailedTeam();
                            PrintOnConsole<DetailedTeam>(ldt);
                            break;
                        case 5:
                            List<UserWithTasks> uwt = communicator.GetUserWithTasks();
                            PrintOnConsole<UserWithTasks>(uwt);
                            break;
                        case 6:
                            id = GetId();
                            DetailedUser du = communicator.GetDetailedUser(id);
                            Console.WriteLine(du);
                            break;
                        case 7:
                            id = GetId();
                            DetailedProject dp = communicator.GetDetailedProject(id);
                            Console.WriteLine(dp);
                            break;
                        case 0:
                            menu = false;
                            break;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Occurs exception: {0}", ex.Message);
                }
                Console.ReadKey();
                Console.Clear();
            } while (menu == true);
        }

        private static void PrintOnConsole<T>(List<T> obj)
        {
            Console.WriteLine("----------------------------------------------------------");
            if (obj != null)
                foreach (var c in obj)
                    Console.WriteLine(c);
            Console.WriteLine("----------------------------------------------------------");
        }

        private static int ShowMenu()
        {
            Console.Write("\tMenu:\n" +
                "1. Get count of tasks for user\n" +
                "2. Get list of tasks for user\n" +
                "3. Get list of finished tasks\n" +
                "4. Get collections of teams\n" +
                "5. Get list of users\n" +
                "6. Get detailed user information\n" +
                "7. Get detailed project information\n" +
                "0. Exit\n" +
                "  Your choice: ");
            int choice = Int32.Parse(Console.ReadLine());
            if (choice < 0 || choice > 8) throw new Exception("Invalid menu item!");
            Console.WriteLine();
            return choice;
        }

        private static int GetId()
        {
            Console.Write("Enter id: ");
            int id = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            return id;
        }
    }
}
